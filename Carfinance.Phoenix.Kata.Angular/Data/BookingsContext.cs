﻿using Carfinance.Phoenix.Kata.Angular.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Carfinance.Phoenix.Kata.Angular.Data
{
    public class BookingsContext : DbContext
    {
        public BookingsContext() : base("DefaultConnection")
        {}
        public virtual DbSet<Booking> Bookings { get; set; }
    }
}