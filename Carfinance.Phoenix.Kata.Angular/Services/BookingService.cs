﻿using Carfinance.Phoenix.Kata.Angular.Models;
using Carfinance.Phoenix.Kata.Angular.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Carfinance.Phoenix.Kata.Angular.Services
{
    public class BookingService : IBookingService
    {

        private readonly IDataService service;
        private static IList<Booking> bookings;

        public BookingService(IDataService dataService)
        {
            this.service = dataService;
            bookings = this.service.Initialize();
        }

        public IList<Booking> GetAllBookings()
        {
            return bookings;
        }

        public void CreateBooking(Booking booking)
        {
            this.ValidateBooking(booking);

            booking.BookingId = bookings.ToList().Select(m => m.BookingId).Max() + 1;
            bookings.Add(booking);

            service.CreateBooking(booking);
        }

        public void UpdateBooking(Booking booking)
        {          
            this.ValidateBooking(booking);
            service.UpdateBooking(booking);       
        }

        private void ValidateBooking(Booking booking)
        {
            if (booking == null)
                throw new ArgumentNullException("booking");

            if (booking.TableNumber <= 0 || booking.TableNumber >= 5)
                throw new ArgumentOutOfRangeException($"Table number {booking.TableNumber} does not exist");
        }

        public Booking GetSingleBooking(int id)
        {
            return this.service.GetBooking(id);
        }
    }
}