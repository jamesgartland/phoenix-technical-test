﻿using Carfinance.Phoenix.Kata.Angular.Models;
using System.Collections.Generic;

namespace Carfinance.Phoenix.Kata.Angular.Services.Interfaces
{
    public interface IDataService
    {
        IList<Booking> Initialize();
        Booking CreateBooking(Booking booking);
        Booking UpdateBooking(Booking booking);
        Booking GetBooking(int id);
    }
}