﻿using Carfinance.Phoenix.Kata.Angular.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Carfinance.Phoenix.Kata.Angular.Models;
using System.Data.Entity;

namespace Carfinance.Phoenix.Kata.Angular.Services
{
    public class EfDataService : IDataService, IDisposable
    {
        private Data.BookingsContext context;

        public Booking CreateBooking(Booking booking)
        {
            if (context == null)
                throw new Exception("Initialize method must be invoked first.");

            context.Bookings.Add(booking);
            context.SaveChanges();
            return booking;
        }

        public IList<Booking> Initialize()
        {
            if (this.context == null)
                this.context = new Data.BookingsContext();

            return context.Bookings.ToList();
        }

        public Booking UpdateBooking(Booking booking)
        {
            if (context == null)
                throw new Exception("Initialize method must be invoked first.");

            var dbBooking = context.Bookings.Find(booking.BookingId);
            dbBooking.BookingTime = booking.BookingTime;
            dbBooking.ContactName = booking.ContactName;
            dbBooking.NumberOfPeople = booking.NumberOfPeople;
            dbBooking.TableNumber = booking.TableNumber;

            context.Entry(dbBooking).State = EntityState.Modified;
            context.SaveChanges();
            return booking;
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        public Booking GetBooking(int id)
        {
            if (context == null)
                throw new Exception("Initialize method must be invoked first.");

            return this.context.Bookings.Find(id);
        }
    }
}