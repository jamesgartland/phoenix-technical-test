﻿using Carfinance.Phoenix.Kata.Angular.Models;
using Carfinance.Phoenix.Kata.Angular.Services;
using Carfinance.Phoenix.Kata.Angular.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Carfinance.Phoenix.Kata.Angular.Controllers
{
    /// <summary>
    /// Your Name:
    /// Date: 
    /// Random fact about you: 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [RoutePrefix("booking")]
    public class BookingController : ApiController
    {
        private readonly IBookingService bookingService;

        public BookingController(IBookingService service)
        {
            this.bookingService = service;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            List<Booking> bookings = bookingService.GetAllBookings().ToList();
            return Ok(bookings.OrderBy(m => m.BookingTime));
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            var booking = bookingService.GetSingleBooking(id);
            if (booking != null)
            {
                return Ok(booking);
            }
            return NotFound();
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]Booking booking)
        {
            if (ModelState.IsValid)
            {
                this.bookingService.CreateBooking(booking);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult Put([FromBody]Booking booking)
        {
            if (ModelState.IsValid)
            {
                this.bookingService.UpdateBooking(booking);
                return Ok();
            }
            return BadRequest(ModelState);
        }


    }
}