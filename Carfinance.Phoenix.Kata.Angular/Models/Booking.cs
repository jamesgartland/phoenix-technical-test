﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Carfinance.Phoenix.Kata.Angular.Models
{
    public class Booking
    {
        [Key]
        public int BookingId { get; set; }
        [Required]
        [Range(1, 5, ErrorMessage = "Table number is invalid")]
        public int TableNumber { get; set; }
        [Required]
        public string ContactName { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string ContactNumber { get; set; }
        [Required]
        public int NumberOfPeople { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime BookingTime { get; set; }
    }
}