namespace Carfinance.Phoenix.Kata.Angular.Migrations
{
    using Carfinance.Phoenix.Kata.Angular.Models;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Hosting;

    internal sealed class Configuration : DbMigrationsConfiguration<Carfinance.Phoenix.Kata.Angular.Data.BookingsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Carfinance.Phoenix.Kata.Angular.Data.BookingsContext context)
        {
            context.Database.CreateIfNotExists();
            JsonConvert.DeserializeObject<List<Booking>>(Carfinance.Phoenix.Kata.Angular.Properties.Resources.initJson).ForEach(booking =>
            {
                context.Bookings.AddOrUpdate(booking);
            });
        }
    }
}
