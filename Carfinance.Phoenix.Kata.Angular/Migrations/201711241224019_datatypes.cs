namespace Carfinance.Phoenix.Kata.Angular.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datatypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bookings", "ContactName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bookings", "ContactName", c => c.String());
        }
    }
}
