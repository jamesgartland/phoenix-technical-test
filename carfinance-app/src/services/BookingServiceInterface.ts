import { Booking } from "../models/Booking";
export interface BookingServiceInterface {
    getAll() : Promise<Array<Booking>>;
    get(id: number): Promise<Booking>;
    create(booking: Booking): Promise<any>;
    update(booking: Booking): Promise<any>;
}