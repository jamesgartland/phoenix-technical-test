import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Booking } from "../models/Booking";
import { BookingServiceInterface } from "./BookingServiceInterface";
import { Config } from '../config';

import 'rxjs/add/operator/map';

@Injectable()
export class BookingService implements BookingServiceInterface {
    
    constructor(@Inject(Config)private config : Config, private http: HttpClient) {}

    /**
     * Pull list of bookings from the API
     */
    public getAll = async () : Promise<Array<Booking>> => {
          return await this.http.get<Array<Booking>>(`${this.config.API_URL}Booking`)
          .toPromise();
    }

    /**
     * Pull a single booking
     */
    public get = async (id: number) : Promise<Booking> => {
          return await this.http.get<Booking>(`${this.config.API_URL}Booking/${id}`)
          .toPromise();
    }

    /**
     * Create a new booking
     */
    public create = async (booking: Booking): Promise<any> => {
        return await this.http.post(`${this.config.API_URL}Booking`,booking, {responseType: 'text' }).toPromise();
    }

    /**
     * Update an existing booking
     */
    public update = async (booking: Booking): Promise<any> => {
        return await this.http.put(`${this.config.API_URL}Booking`,booking, {responseType: 'text' }).toPromise();
    }

}