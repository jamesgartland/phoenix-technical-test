import { Component  } from '@angular/core';


@Component({
  selector: 'app-root',
  template: `
    <main role="main" class="container">
      <router-outlet></router-outlet>
    </main>
  `,
  styles: []
})
export class AppComponent {
  title = 'app';
  constructor(){}
}
