import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxErrorsModule } from '@ultimate/ngxerrors';

import { BookingService } from "../services/BookingService";
import { Config } from "../config";

import { AppComponent } from './app.component';
import { BookingComponent } from "./bookings/bookings.component";
import { BookingFormComponent } from "./bookings/bookingsForm.component";



const appRoutes: Routes = [
  { path: '', redirectTo: 'bookings', pathMatch: 'full' },
  { path: 'bookings', component: BookingComponent},
  { path: 'bookings/:id', component: BookingFormComponent},
  { path: 'bookings/create', component: BookingFormComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BookingComponent,
    BookingFormComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes, { enableTracing: true }
    ),
    FormsModule,
    ReactiveFormsModule,
    BrowserModule, HttpClientModule, NgxErrorsModule
  ],
  providers: [BookingService,Config],
  bootstrap: [AppComponent]
})
export class AppModule { }
