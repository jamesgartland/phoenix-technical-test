import { Component, Inject, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";

import { BookingService } from "../../services/BookingService"
import { Booking } from "../../models/Booking";
import { BookingServiceInterface } from "../../services/BookingServiceInterface";


@Component({
  selector: 'bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingComponent  {
    @Input() bookings: Array<Booking>;

    constructor(@Inject(BookingService) private bookingService : BookingServiceInterface, private router: Router){}
    
    async ngOnInit(){
        this.bookings = await this.bookingService.getAll();
       
    }

    public getBookingClass(booking: Booking) : string{
        if(booking.numberOfPeople > 6)
            return "diners-red";
        if(booking.numberOfPeople == 1)
            return "diners-blue";
    }

    public edit(booking: Booking){
        this.router.navigate([`bookings/${booking.bookingId}`])
    }
 
}
