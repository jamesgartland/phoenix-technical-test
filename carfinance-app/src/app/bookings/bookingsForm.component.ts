import { Component, Inject, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { BookingService } from "../../services/BookingService"
import { Booking } from "../../models/Booking";
import { BookingServiceInterface } from "../../services/BookingServiceInterface";

@Component({
    selector: 'booking-form',
    templateUrl: './bookingsForm.component.html',
    styleUrls: []
})

export class BookingFormComponent {

    private booking: Booking;
    private bookingFormGroup: FormGroup;

    constructor( @Inject(BookingService) private bookingService: BookingServiceInterface, private route: ActivatedRoute, private router: Router) {
        this.bookingFormGroup = new FormGroup({});
        this.booking = {} as Booking;
    }



    async ngOnInit() {
        this.route.params.subscribe(async params => {
            if(!isNaN(+params['id'])) {this.booking = await this.bookingService.get(params['id']);}
            this.bookingFormGroup = new FormGroup({
                'tableNumber': new FormControl(this.booking.tableNumber || null, [Validators.required]),
                'contactName': new FormControl(this.booking.contactName || null, Validators.required),
                'contactNumber': new FormControl(this.booking.contactNumber || null, Validators.required),
                'numberOfPeople': new FormControl(this.booking.numberOfPeople || null, Validators.required),
                'bookingTime': new FormControl(this.booking.bookingTime || null, Validators.required)
            });
        })
    }

    public buildBookingObject() {
        if (this.booking == null) this.booking = {} as Booking;
        this.booking.tableNumber = this.bookingFormGroup.controls['tableNumber'].value;
        this.booking.contactName = this.bookingFormGroup.controls['contactName'].value;
        this.booking.contactNumber = this.bookingFormGroup.controls['contactNumber'].value;
        this.booking.numberOfPeople = this.bookingFormGroup.controls['numberOfPeople'].value;
        this.booking.bookingTime = this.bookingFormGroup.controls['bookingTime'].value;
    }

    public async onSubmit() {
       /// this.buildBookingObject();
        if (this.booking.bookingId != null)
            await this.bookingService.update(this.booking);
        else
            await this.bookingService.create(this.booking);
        this.router.navigate([`bookings`])
    }
}
