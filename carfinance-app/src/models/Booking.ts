
/**
 * Booking Entity 
 * You could use a framework like Swagger to generate these models automatically. 
 */
export interface Booking{
    bookingId : number;
    tableNumber : number;
    contactName : number;
    contactNumber : string;
    numberOfPeople : number;
    bookingTime : Date;    
}